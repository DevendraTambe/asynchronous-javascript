const fs=require('fs');
const data = require('../data/trello-callbacks/boards.json');

const boardData =  (boardId)=>{
    let info = new Promise ((resolve, reject)=>{
        setTimeout(()=>{
            resolve(data.find(item => item.id === boardId));
            eject('Something Wrong');
        },2*1000)
    });

    return info
}

module.exports = boardData;
