const callback2 = require('../trellocallback/callback2.js')
const callback3 = require('../trellocallback/callback3.js')
const boards = require('../data/trello-callbacks/boards.json')
const callback4 = ()=>{
    let info = new Promise((resolve, reject)=>{
        setTimeout(async()=>{
            let boardData = boards.find(item => item.name === 'Thanos');
            let id = boardData.id;
            let listinfo = await callback2(id);
            let listData = listinfo.find(item => item.name === 'Mind');
            let mindId = listData.id;
            let cardsData = await callback3(mindId);
            resolve(cardsData);
        },2*1000)
    });

    return info;
}
module.exports = callback4;