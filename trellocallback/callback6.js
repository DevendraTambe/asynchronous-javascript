const boards = require('../data/trello-callbacks/boards.json');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const callback6 = () => {
    let info = new Promise((resvole, reject) => {
        setTimeout(async () => {
            let boardData = boards.find(object => object.name === 'Thanos');
            let id = boardData.id;

            let listInfo = await callback2(id);
            let allListIds = listInfo.map(object => object.id);

            let allcardsInfo = allListIds.map(async (ids) => {
                let cardsData = await callback3(ids);
                return cardsData;
            });
            const promiseall = Promise.all(allcardsInfo);
            resvole(promiseall);

        }, 2 * 1000);
    });
    return info;
}

module.exports = callback6;