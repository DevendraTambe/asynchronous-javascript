const boards = require('../data/trello-callbacks/boards.json')
const callback2 = require('./callback2.js');
const callback3 = require('./callback3.js');

const callback5 = () =>{
    let info = new Promise((resolve, reject)=>{
        setTimeout(async()=>{
            let boardData = boards.find(item => item.name === 'Thanos');
            let id = boardData.id;

            let listInfo = await callback2(id);
            let listData = listInfo.filter(item=> item.name==='Mind' || item.name === 'Space');
            let userIds = listData.map(item => item.id);

            let cardsInfo  = userIds.map(async (ids)=>{
                let card = await callback3(ids);
                return card;
            });
            const promiseAll = Promise.all(cardsInfo);
            resolve(promiseAll);
        },2*1000);
    });
    return info;
}

module.exports = callback5;