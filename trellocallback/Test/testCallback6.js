const callback6 = require('../callback6')
const callback = async () => {
    try {
        const result = await callback6();
        console.log(result);
    } catch (err) {
        if (err) {
            console.log(err);
        }
    }
}
callback();