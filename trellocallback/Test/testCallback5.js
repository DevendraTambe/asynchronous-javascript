const callback5 = require('../callback5');

const callback = async() =>{
    try {
        const result = await callback5();
        console.log(result);
    }catch(err){
        console.log(err);
    }
}

callback();