const data = require('../data/trello-callbacks/cards.json');

const cardInfo = (id)=>{
    let info = new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve(data[id]);
        },2*1000);
    });
    return info;
}
module.exports = cardInfo;