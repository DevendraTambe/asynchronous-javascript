const fs = require('fs');

const readFile = (filePath) => {
    fs.readFile(`${filePath}`, 'utf8', (err, data) => {
        if (err) {
            throw err;
        }

        let newData = data.toUpperCase();
        let newFileName = 'upperCasedata.txt';
        let newFilePath = './data/fs-callbacks/upperCasedata.txt';

        fs.writeFile(newFilePath, newData, err => {
            if (err) throw err;
            console.log('data is written to new file');
        });

        fs.appendFile('./data/fs-callbacks/filenames.txt', newFileName + '\n', err => {
            if (err) throw err;
            console.log('File name is saved');
        });


    });
}

const readupperCase = (filePath) => {
    //reading file
    fs.readFile(`${filePath}`, 'utf8', (err, data) => {
        if (err) throw err;

        let lowerCaseData = data.toLocaleLowerCase();
        let splitData = lowerCaseData.split(".");
        let sentencesFileName = 'sentences.txt';
        newFilePath2 = './data/fs-callbacks/sentences.txt';

        splitData.forEach((sentence) => {
            fs.appendFile(newFilePath2, sentence + '\n', err => {
                if (err) throw err;
                console.log('Sentence saved \n')
            });
        });

        fs.appendFile('./data/fs-callbacks/filenames.txt', sentencesFileName + '\n', err => {
            if (err) throw err;
            console.log('File name is saved');
        })
    });

}

const readFileNames = (filePath) => {
    fs.readFile(`${filePath}`, 'utf8', (err, data) => {
        if (err) throw err;

        let filesNames = data.split('\n');
        let sortedDataFileName = 'sortedData.txt';

        filesNames.forEach((fName) => {
            if (fName) {
                fs.readFile(`./data/fs-callbacks/${fName}`, 'utf8', (err, data) => {
                    if (err) throw err;
                    fs.appendFile(`./data/fs-callbacks/${sortedDataFileName}`, data + '\n', err => {
                        if (err) throw err;
                        console.log('Data is saved to new file\n');
                    });
                });
            }
        });
        fs.appendFile('./data/fs-callbacks/filenames.txt', sortedDataFileName + '\n', err => {
            if (err) throw err;
            console.log('File name is saved');
        });

    });
}

const deleteFiles = (filePath) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) throw err;

        let fileNames = data.split('\n');

        fileNames.forEach((file) => {
            if (file) {
                fs.unlink(`./data/fs-callbacks/${file}`, err => {
                    if (err) throw err;

                    console.log('files deleted');
                });
            }
        });
    });
}


module.exports = { readFile, readupperCase, readFileNames, deleteFiles };