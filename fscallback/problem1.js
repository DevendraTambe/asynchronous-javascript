const fs = require('fs');

const makeid = require('./randomFileNameGenrator');
const makeDirectory = (dirPath) => {
    fs.mkdir(dirPath, function (err) {
        if (err) {
            console.log(err);
        }
        console.log('Directory Created\n');

    });
};
const makeFile = (dirPath, fileData) => {

    console.log('creating 5 random JSON files.' + `Inside directory ${dirPath}`);

    let num = 0;

    while (num < 5) {
        let fileName = makeid(5);

        fs.writeFile(`${dirPath}/${fileName}.json`, JSON.stringify(fileData), (err) => {
            if (err) throw err;
        });

        num++;
    }

}

const deleteFiles = (dirPath) => {
    fs.readdir(dirPath, function (err, files) {
        if (err) {
            throw err;
        } else {
            if (files.length !== 0) {
                files.forEach(file => {
                    fs.unlink(`${dirPath}/${file}`, (err) => {
                        if (err) throw err;

                        console.log(`${file} was deleted'`);
                    });
                });
            }
            else {
                console.log('There are no files');
            }
        }

    });
}

module.exports = { makeDirectory, makeFile, deleteFiles }