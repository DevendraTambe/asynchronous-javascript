const fs = require('fs');
const problem1 = require('./problem1');

const dirName = 'sample1';

const testObj = {
    day: 'Monday',
    taskCompleted: false,
    joker: 'Coming back'
}
const testing = async (directoryName, inputData) => {
    try {
        await problem1.makeDirectory(directoryName);

        await problem1.makeFile(directoryName, inputData);

        await problem1.deleteFiles(directoryName);

        console.log('Testing successful\n\n');
    } catch (err) {

        console.log('There is some Error', err);
    }
}

testing(dirName, testObj);